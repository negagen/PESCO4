using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneButton : UIButton
{
    [SerializeField] SceneName scene;
    protected override void ButtonMethod()
    {
        SceneManager.LoadScene(scene.ToString());
    }
}