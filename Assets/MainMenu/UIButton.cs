using UnityEngine;
using UnityEngine.UI;

public abstract class UIButton : MonoBehaviour
{
    Button button;

    private void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(ButtonMethod);
    }

    protected abstract void ButtonMethod();
}