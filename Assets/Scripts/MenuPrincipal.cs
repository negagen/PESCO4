﻿
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPrincipal : MonoBehaviour {

    public void BotonJuegoLibre()
    {
        GestionBD.singleton.SetSesionActual("global");
        GestionBD.singleton.SetNivelActual(0);
        SceneManager.LoadScene("Panel");
    }

    public void BotonJuegoControlado()
    {

    }

    public void BotonSalir()
    {
        GestionBD.singleton.SetEstadoSesion(false);
        SceneManager.LoadScene("Inicio");
    }
}
