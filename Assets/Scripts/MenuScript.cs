using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MenuScript : MonoBehaviour
{
    public Button freeGameButton;
    public Button roomsButton;
    public Button creditsButton;

    // Start is called before the first frame update
    void Start()
    {
        freeGameButton.GetComponent<Button>().onClick.AddListener(() => {
            Debug.Log("Juego Libre");
        });
        roomsButton.GetComponent<Button>().onClick.AddListener(() => {
            SceneManager.LoadScene("levelMenu");
        });
        creditsButton.GetComponent<Button>().onClick.AddListener(() => {
            Debug.Log("Credits");
        });
    }
}