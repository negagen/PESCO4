﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class Nivel2Controller : MonoBehaviour {

    public bool juegoGanado = false;
    public GameLogicN2 datosjuego;
    public ModeloN2 modelo;
    //Declaración de variables de control para conocer si ha ganado el juego
    public double peso;
    public double racionPez;
    public double alimentoConsumido;
    public double dinero;
    public double totalVenta;
    public double totalCostos;
    //Control de misiones
    public bool mision1completa;
    public bool mision2completa;
    public bool mision3completa;
    //Avisos de juego ganado o perdido
    public GameObject levelWin;
    public GameObject levelOver;
    public GameObject vendedor;
    void Start()
    {
        mision1completa = false;
        mision2completa = false;
        mision3completa = false;
        datosjuego.GetDineroDisponible();
        modelo.GetModeloActual();
        levelWin.gameObject.SetActive(false);
        levelOver.gameObject.SetActive(false);
    }

    private void Update()
    {
        this.dinero = datosjuego.GetDineroDisponible();
        if (modelo.GetProduccionActiva())
        {
            this.peso = (modelo.GetPesoPromedio());
            this.racionPez = (modelo.GetRacionAlimenticia());
            this.alimentoConsumido = (modelo.GetTotalAlimentoSuministrado());
            this.totalVenta = (datosjuego.GetTotalVenta());
            this.totalCostos = (modelo.GetCostoProduccion());
        }
        if ((int)this.dinero < 1)
        {
            GameOver();
        }
        //Validación misión 1
        if (((float)racionPez) > 0.01f)
        {
            mision1completa = true;
        }
        //Validación de misión 2
        if ((float)alimentoConsumido - 600f < 0.01f)
        {
            mision2completa = true;
        }
        //Validación misión 3
        if ((float)totalVenta-(float)totalCostos > 0.01f)
        {
            mision3completa = true;
        }

    }

    public void ValidarJuego()
    {
        print("Ración:" + (int)racionPez + " - Total alimento:" + (int)alimentoConsumido);
        if (mision1completa && mision2completa && mision3completa)
        {
            StartCoroutine(GameWin());
            GestionBD.singleton.RegistrarFinalizacionNivel();
        }
        else
        {
            GameOver();
        }
    }

    IEnumerator GameWin()
    {
        //peso = Double.Parse(produccion.GetComponent<PescoModelo>().datosModelo[0]);
        Debug.Log("GANASTE----------------->");
        vendedor.GetComponent<Animator>().SetTrigger("venta");
        yield return new WaitForSeconds(3f);
        //GetComponent<AudioSource>().Play();
        GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>().enabled=false;
        levelWin.SetActive(true);
        juegoGanado = true;
    }

    private void GameOver()
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>().enabled = false;
        levelOver.SetActive(true);
    }
}
