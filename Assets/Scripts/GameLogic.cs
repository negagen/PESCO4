﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/**
 * Organiza los valores actuales de la partida para enviarlos al componente
 * Almacenamiento y este se encargue de guardarlos.
 *
 * Posiciones:
 * 1: Nombre de usuario.
 * 2: Dinero del usuario.
 * 3: Dias transcurridos. (iteraciones)
 * 4: Estado produccion Activa(0 = false, 1 = true).
 * 5: Experiencia del jugador
 * 6: Nivel (1 o 2)
 * 7: Sesion (nombre de la sesion)
 * 8: Tiempo Iteracion
 * 9: Cambio de Nivel (0 = false, 1 = true).
 * 10-15: Cinco mejores puntajes de venta.
 * 15-26: Datos del Modelo.
 */


public class GameLogic : MonoBehaviour {

    /*En este sccript se colocan todas las acciones lógicas que producen un cambio en el juego
    Efectos de pulsar los botones*/
    public bool isPaused = false;
    public AudioMixerSnapshot pausado;
    public AudioMixerSnapshot corriendo;
    public AudioMixerSnapshot bajoagua;
    public GameObject prefab;
   //public GameObject modelo;
    public GameObject produccion;
    //Elementos del panel de insumos
    public GameObject mensajePanel;
    public string mensaje;
    //Estado de la produccion
    public bool estado=false;

    /*
    private int tiempoIteracion = 0;
    private final double DINEROINICIAL = 10000;
    private double dineroJugador;
    private double experiencia;
    private int diasTranscurridos = 0;
    private double[] mejoresPuntajes = { 0, 0, 0, 0, 0 };
    private String[] registrosRMS;
    private String[] datosModelo;
    private ModeloCompuesto ModeloCompuesto;
    private Conexion conexionServidor;
    private boolean pantallaCompleta = false;
    private String nombreUsuario;
    private String[] itemsMenuPrincipal;
    private String[] itemsMenuSesion;
    private String[] itemsMenuOpciones;
    private String[] itemsMenuNivel;
    private Date tiempoActual;
    private boolean produccionActiva = false;
    private String nuevaCadena;
    private String Sesion = null;
    private FuentePesco fuentePesco;
    private boolean mostrarNiveles = false;*/

    //public GameObject produccion;


    private void Awake()
    {
        Time.timeScale = 1;
    }

    // Use this for initialization
    void Start () {
        mensaje = "";
        isPaused = false;
        //Instantiate(prefab);
	}

    // Update is called once per frame
    void Update()
    {
        //La lógica del juego debe estar mirando si hay una producción activa.
        estado = produccion.GetComponent<ModeloN1>().GetProduccionActiva();
        if (estado == false)
        {
            //Debug.Log("No hay peces creciendo");
        }
        //Debo mirar si hay una producción activa, si la hay no puede comprar, sólo vender.
        //if (prefab.GetComponent<PescoModelo>().produccionActiva == true)
        {
            //;
        }

        if (Input.GetKeyDown("p"))
        {
            Pause();
            //Pause();
        }
    }

    public void LateUpdate()
    {
        mensajePanel.GetComponent<Text>().text = mensaje;
    }

    //Función para Pausar
    public void Pause()
    {
        if (isPaused == true)//¿El juego está pausado?
        {//Hagalo correr
            Time.timeScale = 1;
            corriendo.TransitionTo(0.01f);
            isPaused = false;
        }
        else if(isPaused==false)
        {//Hagalo pausar
            Time.timeScale = 0;
            pausado.TransitionTo(0.01f);
            isPaused = true;
        }
    }

    public void ComprarPeces()
        //Si hay produccion activa no puedo comprar;
    {
               
        //Puedo activar el gameObject que contiene los peces y la lógica de crecimiento de esos peces
        if (estado==true)
        {
            mensaje = "No tiene producción, puede comprar una nueva.";
            //Debug.Log("No tiene producción, puede comprar una nueva.");
            //produccion.gameObject.SetActive(true);
        }
        else
        {
            mensaje = "Tiene una producción activa, por tanto no puede comprar.";
            Debug.Log("Tiene una producción activa, por tanto no puede comprar.");
            
            Debug.Log("Compra efectuada a los " + Time.time + ".");
            produccion.SetActive(true);
            produccion.GetComponent<ModeloN1>().timer.CorrerTiempo();
            //Activa una nueva producción nueva
            produccion.GetComponent<ModeloN1>().SetProduccionActiva(true);
            //Activar un reloj
           
        }
        //O puedo crear un prefab
        //Instantiate(peces).SetActive(true);
        //GameObject.Instantiate(modelo).SetActive(false);
        //GameObject.Instantiate(peces).SetActive(false);
        //modelo.gameObject.SetActive(true);
        //modelo.GetComponent<PescoModelo>().produccionActiva = true;

        //Inicia un nuevo modelo
        //Instantiate(prefab).SetActive(true);

    }

    public void ComprarPeces(int numPeces)
    //Si hay produccion activa no puedo comprar;
    {

        //Puedo activar el gameObject que contiene los peces y la lógica de crecimiento de esos peces
        if (estado == true)
        {
            mensaje = "No tiene producción, puede comprar una nueva.";
            //Debug.Log("No tiene producción, puede comprar una nueva.");
            //produccion.gameObject.SetActive(true);
        }
        else
        {
            mensaje = "Tiene una producción activa, por tanto no puede comprar.";
            Debug.Log("Tiene una producción activa, por tanto no puede comprar.");

            Debug.Log("Compra efectuada a los " + Time.time + ".");
            produccion.SetActive(true);
            produccion.GetComponent<ModeloN1>().timer.CorrerTiempo();
            //Activa una nueva producción nueva
            produccion.GetComponent<ModeloN1>().SetProduccionActiva(true);
            //Activar un reloj

        }
        //O puedo crear un prefab
        //Instantiate(peces).SetActive(true);
        //GameObject.Instantiate(modelo).SetActive(false);
        //GameObject.Instantiate(peces).SetActive(false);
        //modelo.gameObject.SetActive(true);
        //modelo.GetComponent<PescoModelo>().produccionActiva = true;

        //Inicia un nuevo modelo
        //Instantiate(prefab).SetActive(true);

    }

    public void VenderPeces()
    {
        //Guardo los datos de venta y luego acabo el modelo y los peces en el estanque
        //Debo mirar si se trata de una venta parcial o completa del lote, esto lo indicará el número de peces en lote
        //Reinicio el tiempo

        //modelo.SetActive(false);
        produccion.GetComponent<ModeloN1>().SetProduccionActiva(false);
        //produccion.SetActive(false);
        estado = false;
        mensaje = "No tiene producción, puede comprar una nueva.";
        Debug.Log("Venta realizada a los "+Time.time+".");

        /*GameObject MyObjName = GameObject.Find("PecesEnProduccion");
        foreach (Transform child in MyObjName.transform)
        {
            child.gameObject.SetActive(false);
        }*/

        /*foreach (Behaviour childCompnent in produccion.GetComponentsInChildren<Behaviour>())
            childCompnent.enabled = false;*/
        //produccion.gameObject.SetActive(false);
        //produccion.GetComponent<PescoModelo>().produccionActiva = false;

        //SceneManager.LoadScene("EstanquePeces");

    }

    public void ReiniciarNivel()
    {
        //Recargue la escena
        Scene nivelActual = SceneManager.GetActiveScene();
        Debug.Log("Reiniciando el " + nivelActual.name);

        SceneManager.LoadScene(nivelActual.name,LoadSceneMode.Single);
        //Time.timeScale = 1;

        //SceneManager.UnloadSceneAsync(nivelActual.name);
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        //Scene scene = SceneManager.GetActiveScene();
        //Debug.Log("Active scene is '" + scene.name + "'.");
    }

    public void VolverHome()
    {
        SceneManager.LoadScene(0);
    }

}
