﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class UIControllerN2 : MonoBehaviour {

    //Modelo vigente
    public GameLogicN2 datosjuego;
    public ModeloN2 modelo;
    public Text nombreJugador;
    public Text nombreSesion;
    public Text HUDNivel;
    public Image HUDLogoPesco;
    //Campos que cambian del HUD
    public Text HUDPeces;
    public Text HUDDinero;
    public Text HUDDia;
    //Campos que cambian de la UI
    //Campos que cambian de los paneles de cada escenario del mapa
    //Panel del Estanque
    public Text PEPeso;
    public Text PEConsumoActual;
    public Text PEFactorConversion;
    public Text PECosto;
    public Text PECostoKg;
    //PE - Pestaña Ración
    public Text PERacionRequerida;
    public Text PERacionASuministrar;
    public Text PERacionSuministrada;
    public Text PETotalAlimentoSuministrado;
    //Panel del Mercado
    public Text PMCostosTotales;
    public Text PMBiomasa;
    public Text PMCostoKg;
    public Text PMPrecioKg;
    private string vacio = "...";

    //Módulo de duración del día
    public Text SDDValorDia;

    //Botones que cambian del juego
    public Button btnCambiarRacion;

    //Mensajes de error y alerta
    private bool controlAnimacionProduccion;
    public Image imgErrorPesoMinimo;
    public Image imgErrorConexion;
    public Image imgGuardarEstado;

    // Use this for initialization
    void Start () {
        modelo.GetModeloActual();
	}
	
	// Update is called once per frame
	void Update () {
        ReproducirAnimacionInicioProduccion(datosjuego.GetEstadoProduccion());
        //Suministro ración
        if (datosjuego.GetEntradaRacionNula() == true)
        {
            VerOpcionesSuministroRacion(false);
        }
        else
        {
            VerOpcionesSuministroRacion(true);
        }
        //Guardando partida
        if (datosjuego.GetGuardandoPartida())
        {
            imgGuardarEstado.gameObject.SetActive(true);
        }
        else
        {
            imgGuardarEstado.gameObject.SetActive(false);
        }
    }

    private void OnGUI()
    {
        nombreJugador.text = GestionBD.singleton.GetNombreJugador();
        nombreSesion.text = GestionBD.singleton.GetNombreSesion();
        HUDNivel.text = "NIVEL 2";
        //HUDDinero.text = datosjuego.GetDineroDisponible().ToString();
        HUDDinero.text = datosjuego.GetDineroDisponible().ToString("N2");
        PMPrecioKg.text = "$ " + datosjuego.GetPrecioVentaPez().ToString("N0");
        //SDDValorDia.text = modelo.timer.getDuracionDia().ToString();
        //Actualización de textos en el juego
        if (modelo.GetProduccionActiva() == true)
            {
            //Actualización de datos del HUD
            HUDPeces.text = modelo.GetPecesEnLoteP().ToString();
            HUDDia.text = modelo.GetTiempoIteracion().ToString();
            //Actualización de datos de Paneles
            PEPeso.text = AjustarGramos(modelo.GetPesoPromedio());
            PEConsumoActual.text = AjustarGramos(modelo.GetRacionAlimenticiaReal());
            PEFactorConversion.text = Math.Round(modelo.GetFactorConversion(), 3).ToString();
            PECosto.text = "$ " + modelo.GetCostoProduccion().ToString();
            PECostoKg.text = "$ " + Math.Round(((modelo.GetCostoProduccion()) / (modelo.GetPesoPromedio()/1000f)),2).ToString("N0"); ;

            PERacionRequerida.text = AjustarGramos(modelo.GetRacionRequerida());
            PERacionASuministrar.text = AjustarGramos(modelo.GetRacionAlimenticia());
            PERacionSuministrada.text = AjustarGramos(modelo.GetRacionSuministrada());
            PETotalAlimentoSuministrado.GetComponent<Text>().text = AjustarGramos(modelo.GetTotalAlimentoSuministrado());

            //PECostoGr.text= "$ " + Math.Round(((modelo.GetCostoProduccion()) / (modelo.GetPesoPromedio())), 2).ToString();
            PMCostosTotales.text = "$ " + modelo.GetCostoProduccion().ToString();
            PMBiomasa.text = AjustarGramos(modelo.GetPesoPromedio());
            PMCostoKg.text = "$ " + Math.Round(((modelo.GetCostoProduccion()) / (modelo.GetPesoPromedio()/1000f)), 2).ToString("N0");
            
            SDDValorDia.text = modelo.timer.GetDuracionDia().ToString();
        }
        else
        {
            //Actualización de datos del HUD
            HUDPeces.text = "-";
            HUDDia.text = "-";
            HUDDia.gameObject.SetActive(false);
            HUDLogoPesco.gameObject.SetActive(true);
            //Actualización de datos de Paneles
            PEPeso.text = vacio;
            PEFactorConversion.text = vacio;
            PEConsumoActual.text = vacio;
            PECosto.text = vacio;
            PECostoKg.text = vacio;
            PMCostosTotales.text = vacio;
            PMBiomasa.text = vacio;
            PMCostoKg.text = vacio;
            PERacionRequerida.text = vacio;
            PERacionASuministrar.text = vacio;
            PETotalAlimentoSuministrado.text = vacio;
        }
    }

    private void ReproducirAnimacionInicioProduccion(bool estado)
    {
        if (estado)
        {
            if (controlAnimacionProduccion)
            {
                StartCoroutine(IniciarAnimacionProduccion());
                controlAnimacionProduccion = false;
            }
        }
        else
        {
            controlAnimacionProduccion = true;
        }
    }

    IEnumerator IniciarAnimacionProduccion()
    {
        HUDDia.gameObject.SetActive(false);
        HUDLogoPesco.gameObject.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(3f);
        controlAnimacionProduccion = false;
        HUDLogoPesco.gameObject.SetActive(false);
        HUDDia.gameObject.SetActive(true);
    }

    private void VerOpcionesSuministroRacion(bool activo)
    {
        btnCambiarRacion.gameObject.SetActive(activo);
    }

    private string AjustarGramos(double valor)
    {
        string mensaje;
        if (valor < 1000)
        {
            mensaje = Math.Round(valor, 2).ToString() + " g";
        }
        else
        {
            mensaje = Math.Round(valor / 1000f, 3).ToString() + " kg";
        }
        return mensaje;
    }

    private string AjustarPesos(double valor)
    {
        string valorAMostrar = valor.ToString("c");
        return valorAMostrar;
    }
}
