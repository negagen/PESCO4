﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    public float Velocidad = 5.0F;
    public float speed;
    public Text countText;
    public Text winText;

	private Rigidbody rb;

    public bool derecha = false;
    public bool izquierda = false;
    public bool arriba = false;
    public bool abajo = false;

    void Start()
	{
		rb = GetComponent<Rigidbody> ();/*
        count = 0;
        //SetCountText();
        //winText.text = "";      */
    }

    void Update()
    {

        //Método de desplazamiento inteligente
        
            //GetComponent<NavMeshAgent>().enabled = false;
            //GetComponent<AgentScript>().enabled = false;
            
                //Control con teclado
        /*if (Input.GetKey(KeyCode.UpArrow))
         {

            //MoverArriba();
            arriba = true;
            /*abajo = false;
            izquierda = false;
            derecha = false;
    }
         else if (Input.GetKey(KeyCode.DownArrow))
         {
             //MoverAbajo();
             abajo = true;
         }
         else if (Input.GetKey(KeyCode.LeftArrow))
         {
             //MoverIzqda();
             izquierda = true;
         }
         else if (Input.GetKey(KeyCode.RightArrow))
         {
             //MoverDerecha();
             derecha = true;
         }else
         {
             //Detener();
             derecha = false;
            izquierda = false;
            arriba = false;
            abajo = false;
         }
         

        //Control Táctil
        if (derecha)
        {
            transform.Translate(Vector3.right * Time.deltaTime * Velocidad);
        }

        if (izquierda)
        {
            this.transform.Translate(Vector3.left * Time.deltaTime * Velocidad);
        }

        if (arriba)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * Velocidad);
        }

        if (abajo)
        {
            this.transform.Translate(Vector3.back * Time.deltaTime * Velocidad);
        }
        */
    }

    public void MoverDerecha()
    {
        derecha = true;
    }

    public void MoverIzqda()
    {
        izquierda = true;
    }

    public void MoverArriba()
    {
        arriba = true;
    }

    public void MoverAbajo()
    {
        abajo = true;
    }

    public void Detener()
    {
        derecha = false;
        izquierda = false;
        arriba = false;
        abajo = false;
    }

    void FixedUpdate() {
       
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rb.AddForce (movement*speed);
	}
}