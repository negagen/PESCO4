﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {
	
    //Utilizado para hacer girar la moneda de información

	// Update is called once per frame
	void Update () {
        transform.Rotate(new Vector3(0, 90, 0) * Time.deltaTime);
	}
}
