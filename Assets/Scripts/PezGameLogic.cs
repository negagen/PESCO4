﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine;

public class PezGameLogic : MonoBehaviour
{
    /*En este script se colocan todas las acciones que producen un cambio en el juego
        Efectos de pulsar los botones*/
    public float temperatura;
    public GameObject UITemperatura;
    public GameObject UISolucion;
    public int posicioncorrecta;
    public string solucion;
    public GameObject tiempo;
    public GameObject UIPanelRespuesta;

    
    //Posición de las opciones
    Vector3 izquierda;
    Vector3 centro;
    Vector3 derecha;
    public GameObject correctoPrefab;
    public GameObject incorrectoPrefab;
    public bool isPaused = false;
    public bool estado = false;



    // Use this for initialization
    void Start()
    {
        izquierda = new Vector3(-10, 5, 12);
        centro = new Vector3(0, 5, 12);
        derecha = new Vector3(10, 5, 12);
        GenerarTemperatura();
    }

    // Revisar si está en la posición correcta
    void Update()
    {
        if (Input.GetKeyDown("p"))
        {
            Pause();
        }
        if (tiempo.GetComponent<PezCountdown>().timeover == true)
        {
            UIPanelRespuesta.SetActive(true);
        }
    }

    private void LateUpdate()
    {
        UITemperatura.GetComponent<Text>().text = temperatura.ToString();
        UISolucion.GetComponent<Text>().text=solucion;
    }

    private void GenerarTemperatura()
    {
        //Generar un random entre los límites de temperatura de la tabla
        temperatura = Mathf.Round(Random.Range(16.0f, 32.0f) * 100f) / 100f;
        Debug.Log(temperatura);
        //Consulto la tabla del efecto de la temperatura y ésta retorna el valor de EfectodelaTemperatura(double)
        double efectotemp;
        efectotemp = PescoMultiplicadorEstanque.EfectTempAlim(temperatura);
        ValidarRespuesta(efectotemp);
    }

    private void ValidarRespuesta(double efectotemp)
    {
        //Dependiendo del valor de efectotemp se conoce si hay que cambiar la ración, si es 1 se mantiene, si es diferente de 1 se cambia.
        if (efectotemp == 1)
        {
            posicioncorrecta = 0;
            solucion = "La respuesta correcta es mantener.";
            Debug.Log("La respuesta correcta es mantener.");

            GameObject.Instantiate(incorrectoPrefab, izquierda, Quaternion.Euler(0, 0, 0));
            GameObject.Instantiate(correctoPrefab, centro, Quaternion.Euler(0, 0, 0));
            GameObject.Instantiate(incorrectoPrefab, derecha, Quaternion.Euler(0, 0, 0));
        }else
        {
            solucion = "La respuesta correcta es cambiar.";
            Debug.Log("La respuesta correcta es cambiar.");
            GameObject.Instantiate(correctoPrefab, izquierda, Quaternion.Euler(0, 0, 0));
            GameObject.Instantiate(incorrectoPrefab, centro, Quaternion.Euler(0, 0, 0));
            GameObject.Instantiate(correctoPrefab, derecha, Quaternion.Euler(0, 0, 0));
        }
    }


    //Función para Pausar
    void Pause()
    {
        if (isPaused == true)//¿El juego está pausado?
        {//Hagalo correr
            Time.timeScale = 1;
            //corriendo.TransitionTo(0.01f);
            isPaused = false;
        }
        else
        {//Hagalo pausar
            Time.timeScale = 0;
            //pausado.TransitionTo(0.01f);
            isPaused = true;
        }
    }


}
