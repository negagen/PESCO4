using UnityEngine;
using System.Runtime.InteropServices;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class AuthService{
    private bool Loading;
    private static JwtToken token;

    public static IEnumerable login(string username, string password){
        LoginDTO authDto = new LoginDTO();

        authDto.username = username;
        authDto.password = password;

        #if UNITY_EDITOR

        yield return new WaitForSeconds(1);
        token.access_token = "1234567890";

        #else
        // call the web api to get the token
        UnityWebRequest www = UnityWebRequest.Post("http://localhost:3000/api/auth/login", "username=" + username + "&password=" + password);
        yield return www.SendWebRequest();

        if((www.result == UnityWebRequest.Result.ConnectionError) || (www.result == UnityWebRequest.Result.ProtocolError)){
            Debug.Log(www.error);
        }
        else{
            Debug.Log(www.downloadHandler.text);
            token.access_token = JsonUtility.FromJson<JwtToken>(www.downloadHandler.text).access_token;
        }

        #endif
    }

    public static void logout(){

    }
}