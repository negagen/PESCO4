using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExpandText : MonoBehaviour
{
    public float sizeMore = 230;
    public float sizeLess = 150;
    public Text text;

    private string msjMore = "Leer mas";
    private string msjLess = "Leer menos";
    private int state = 0;//0 leer mas, 1 leer menos
    private Vector3 defaultPositionParent;

    public void Start()
    {
        RectTransform parent = (RectTransform)transform.parent;
        defaultPositionParent = parent.anchoredPosition3D;
    }

    public void Expand()
    {
        if(state == 0)
        {
            state = 1;
            text.text = msjLess;
            RectTransform parent = (RectTransform)transform.parent;
            parent.anchoredPosition3D = new Vector3(parent.anchoredPosition.x, parent.anchoredPosition.y + ((sizeMore-sizeLess)/2), parent.anchoredPosition3D.z);
            parent.sizeDelta = new Vector2(parent.sizeDelta.x, sizeMore);
        }
        else
        {
            state = 0;
            text.text = msjMore;
            RectTransform parent = (RectTransform)transform.parent;
            parent.sizeDelta = new Vector2(parent.sizeDelta.x, sizeLess);
            parent.anchoredPosition3D = defaultPositionParent;
        }
    }
}
