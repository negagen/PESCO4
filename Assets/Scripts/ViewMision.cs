﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class ViewMision : MonoBehaviour
{

    public Image vistaMisiones;
    public Text mision1UI;
    public Text mision2UI;
    public Text mision3UI;
    public int nivelactual;

    private string mision1;
    private string mision2;
    private string mision3;
    // Use this for initialization
    void Start()
    {
        nivelactual = GestionBD.singleton.GetNivel();
        mision1 = "";
        mision2 = "";
        mision3 = "";
        //vistaMisiones GameObject.FindGameObjectWithTag("Main Camera");
        if (nivelactual == 1)
        {
            mision1= "Vende tu pez cuando tenga un peso mayor a 50 gramos.";
            mision2 = "Vende tu pez cuando la ración requerida sea mayor a 2 gramos.";
            mision3 = "Evita que el dinero disponible se agote.";
        }
        if (nivelactual == 2)
        {
            mision1 = "Cambiar la ración alimenticia del pez.";
            mision2 = "Vende tu pez antes de que el total de alimento consumido sea mayor a 600 gramos.";
            mision3 = "Vende obteniendo utilidad.";
        }
        if (nivelactual == 3)
        {
            mision1 = "Administra el alimento de forma que no se agote.";
            mision2 = "Realiza una compra de 10 peces.";
            mision3 = "Cambia la ración alimenticia en el estanque.";
        }
        if (nivelactual == 4)
        {
            mision1 = "Realiza una compra de alimento";
            mision2 = "Realiza una compra de mínimo 10 peces.";
            mision3 = "Cambia la ración alimenticia en el estanque.";
        }
        if (nivelactual == 5)
        {
            mision1 = "Realiza una venta parcial de peces.";
            mision2 = "Cambia la ración alimenticia en el estanque.";
            mision3 = "Vende cuando los peces tengan un peso superior a 125 gramos.";
        }
        if (nivelactual == 6)
        {
            mision1 = "Selecciona una modalidad de venta.";
            mision2 = "Evita que el dinero disponible se agote.";
            mision3 = "Vende cuando los peces tengan un peso superior a 125 gramos.";
        }
        if (nivelactual == 7)
        {
            mision1 = "Selecciona una modalidad de venta.";
            mision2 = "Evita que el dinero disponible se agote.";
            mision3 = "Vende cuando los peces tengan un peso superior a 125 gramos.";
        }

    }

    private void Update()
    {
        mision1UI.GetComponent<Text>().text = mision1;
        mision2UI.GetComponent<Text>().text = mision2;
        mision3UI.GetComponent<Text>().text = mision3;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //Mostrar panel
            vistaMisiones.gameObject.SetActive(true);
            //Ocultar ícono
            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<AudioSource>().Play();
            //gameObject.GetComponent<>
        }
    }

    void OnTriggerExit(Collider other)
    {
        //Ocultar panel
        vistaMisiones.gameObject.SetActive(false);
        //Mostrar ícono
        GetComponent<MeshRenderer>().enabled = true;
    }
    
}
