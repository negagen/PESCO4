﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PlayTimer : MonoBehaviour {

    //Variables de duración
    public int playtime;
    private int seconds;
    private int minutes;
    private int hours;
    private int days;
    public float tiempoRestante;

    //Variables de control del tiempo
    [SerializeField]
    private bool tiempoDetenido;

    //Variables para enviar al modelo
    [SerializeField]
    private int duracionDia;
    public int diasTranscurridos;
    [SerializeField]
    private int totalDias;

    //Variables del UI
    public Text duracionDiatxt;
    public InputField entradaDia;


    // Use this for initialization
    private void Awake()
    {
        seconds = 0;
        minutes = 0;
        hours = 0;
        days = 0;
        diasTranscurridos = 0;
        totalDias = 0;
        duracionDia = 10;
    }

    void Start()
    {
        //duracionDia = 10;
        //correrTiempo();
      
    }

	
    private IEnumerator Playtimer()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            playtime += 1;
            seconds = (playtime%60);
            minutes = (playtime / 60) % 60;
            hours = (playtime/3600)%24;
            days = (playtime/86400)%365;
            //Si el tiempo de duración del día es fijo
            diasTranscurridos = (playtime/duracionDia);
            //Si el tiempo de duración del día es variable
            if (playtime % duracionDia == 0)
            {
                totalDias += 1;
            }
            //tiempo para finalizar día de simulación
            tiempoRestante = duracionDia - (playtime%duracionDia);
        }
    }

    /*Función para visualizar en pantalla los datos de tiempo
     * private void OnGUI()
    {
        GUI.color = Color.black;
        GUI.Label(new Rect(50, 10, 400, 50), "Playtime:" + days.ToString() + " d " + hours.ToString() + " h " + minutes.ToString() + " m " + seconds.ToString() + " s" + "-->"+playtime.ToString());
        GUI.Label(new Rect(50, 30, 400, 50), "Duración Día:"+duracionDia.ToString()+" s");
        GUI.Label(new Rect(50, 50, 400, 50), "Días Transcurridos:" + diasTranscurridos.ToString());
        GUI.Label(new Rect(50, 70, 400, 50), "Total Días" + totalDias.ToString());
    }*/

    // Update is called once per frame
    void Update() {
        //duracionDiatxt.text = getDuracionDia().ToString();
        //diasTranscurridos = (playtime / duracionDia)%duracionDia;

    }

    public void CorrerTiempo()
    {
        StartCoroutine("Playtimer");
        tiempoDetenido = false;
    }

    public void ReiniciarTiempo()
    {
        //StopAllCoroutines();
        StopCoroutine("Playtimer");
        playtime = 0;
        seconds = 0;
        minutes = 0;
        hours = 0;
        days = 0;
        diasTranscurridos = 0;
        totalDias = 0;
        tiempoDetenido = true;
    }


    //Métodos Getter
    int GetDiasTranscurridos()
    {
        return diasTranscurridos;
    }

    public int GetDuracionDia()
    {
        return duracionDia;
    }

    public int GetTotalDias()
    {
        return totalDias;
    }

    public int GetPlaytime()
    {
        return playtime;
    }

    public bool GetTiempoDetenido()
    {
        return tiempoDetenido;
    }
    //Métodos Setter
    public void SetDuracionDia()
    {
        //Time.timeScale = 0;
        int nuevaduracion;
        nuevaduracion = int.Parse(entradaDia.text);
        //Ajuste de la diferencia de tiempo
        if (nuevaduracion >= 1)
        {
            int ajuste;
            ajuste = playtime % duracionDia;
            int diferenciaDias;
            diferenciaDias = ajuste / nuevaduracion;
            Debug.Log("Diferencias de dias es-->" + diferenciaDias);
            totalDias = totalDias + diferenciaDias;
            //Asignación de nueva duración del día
            duracionDia = nuevaduracion;
            playtime = ajuste % nuevaduracion;
            //Time.timeScale = 1;
        }
    }

    public void FijarDuracionDia(int duracion)
    {
        this.duracionDia = duracion;
    }

    public void SetTotalDias(int tiempo)
    {
        this.totalDias=tiempo;
    }

}
